#include <SPI.h>
#include <Wire.h>
#include <Adafruit_SSD1306.h>
#include <WiFi.h>
#include <HTTPClient.h>
#include <ArduinoJson.h>

/** OLED **/
#define OLED_ADDR 0x3C
#define OLED_WIDTH 128
#define OLED_HEIGHT 64
#define OLED_RESET -1
Adafruit_SSD1306 display(OLED_WIDTH, OLED_HEIGHT, &Wire, OLED_RESET);

/** WIFI **/
#define SSID "SSID"
#define PASSWORD "PASSWORD"

void setup(){
    Serial.begin(9600);
    delay(1000);
    Serial.println("\n");

    if(!display.begin(SSD1306_SWITCHCAPVCC, OLED_ADDR)) {
      Serial.println(F("SSD1306 allocation failed"));
      exit(1);
    }
    Serial.println(F("SSD1306 allocation OK"));
    display.display(); //Init screen

    WiFi.persistent(false);
    WiFi.begin(SSID, PASSWORD);
    Serial.print("Tentative de connexion...");
    display.clearDisplay();
    display.setCursor(1,1);
    display.setTextSize(1);
    display.setTextColor(SSD1306_WHITE);
    display.print("Tentative de connexion sur " + SSID +"...");
    display.display();

    while (WiFi.status() != WL_CONNECTED){
        Serial.print(".");
        delay(500);
    }

    Serial.println("\n");
    Serial.println("Connexion etablie!");
    Serial.print("Adresse IP: ");
    Serial.println(WiFi.localIP());
    display.clearDisplay();
    display.setCursor(1,1);
    display.setTextSize(2);
    display.setTextColor(SSD1306_WHITE);
    display.print(WiFi.localIP());
    display.display();
    delay(2000);
}

void loop(){

    HTTPClient http;
 
    http.begin("http://worldtimeapi.org/api/timezone/Europe/Paris");
    int httpCode = http.GET();
 
    if (httpCode > 0) { //Check for the returning code
 
      String payload = http.getString();
      StaticJsonDocument<5000> doc;
      DeserializationError error = deserializeJson(doc, payload);
      if (error) {
        Serial.print(F("deserializeJson() failed: "));
        Serial.println(error.c_str());
        return;
      }

      String datetime = doc["datetime"];
      String MTime = datetime.substring(11, 16);
      display.clearDisplay();
      display.setTextSize(4);
      display.setTextColor(SSD1306_WHITE);
      display.setCursor(5, 10);
      display.println(MTime);
      display.display(); 
              
    }else {
      Serial.println("Error on HTTP request");
    }
    http.end();
    delay(1000);
}